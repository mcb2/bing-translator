package mcb.BingTranslator;

import jade.core.Profile;
import jade.core.ProfileException;
import jade.core.ProfileImpl;

public class Main {
    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("You must specify the config file path as first argument.");
        }

        try {
            Profile profile = new ProfileImpl(args[0]);
        } catch (ProfileException e) {
            e.printStackTrace();
        }

    }
}
