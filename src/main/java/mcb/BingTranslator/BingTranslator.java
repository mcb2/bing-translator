package mcb.BingTranslator;

import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;
import jade.core.AID;
import jade.core.Agent;
import jade.core.ServiceException;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.messaging.TopicManagementHelper;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.Properties;

// https://code.google.com/p/microsoft-translator-java-api/
public class BingTranslator extends Agent {
    public void extractConfigsOrFail(Object[] args) {
        if (args.length < 2) {
            throw new IllegalArgumentException("You must specify the bing client-id, client-secret, and optionally an HTTP Proxy server and ports as arguments, in this order.");
        }

        Translate.setClientId(args[0].toString());
        Translate.setClientSecret(args[1].toString());

        if (args.length == 4) {
            Properties systemProperties = System.getProperties();
            String proxyServer = args[2].toString();
            String proxyPort = args[2].toString();
            systemProperties.setProperty("http.proxyHost", proxyServer);
            systemProperties.setProperty("http.proxyPort", proxyPort);
            System.out.format("Proxy set to %s:%s%n", proxyServer, proxyPort);
        }
    }

    public void setup() {
        // need : proxy setting, bing key
        extractConfigsOrFail(getArguments());

        TopicManagementHelper topicHelper = null;
        try {
            topicHelper = (TopicManagementHelper) getHelper(TopicManagementHelper.SERVICE_NAME);
        } catch (ServiceException e) {
            System.err.println("Could not get TopicHelperService");
            e.printStackTrace();
            this.doDelete();
            return;
        }

        AID translatorTopic = topicHelper.createTopic("Translation");
        try {
            topicHelper.register(translatorTopic);
        } catch (ServiceException e) {
            System.err.println("Could not register Translation topic" + translatorTopic.getLocalName());
            e.printStackTrace();
            this.doDelete();
            return;
        }

        final MessageTemplate translationDemandsMessages = MessageTemplate.MatchTopic(translatorTopic);
        addBehaviour(new CyclicBehaviour(this) {
            @Override
            public void action() {
                final ACLMessage msg = myAgent.receive(translationDemandsMessages);

                if (msg != null) {
                    if (msg.getPerformative() == ACLMessage.REQUEST) {
                        msg.createReply();
                        msg.setContent("42");

                        String request = msg.getContent();
                        String[] parts = request.split(";");
                        if (parts.length < 3) {
                            ACLMessage reply = msg.createReply();
                            reply.setContent("I could not read your request. A translation request should be in the format SOURCE_LANG;TARGET_LANG;text.");
                            reply.setPerformative(ACLMessage.NOT_UNDERSTOOD);
                            myAgent.send(reply);
                        }

                        Language sourceLang = findLang(parts[0]); // language name in english, full capitals
                        Language targetLang = findLang(parts[1]);

                        // todo, check if translation is possible by checking in our list of available languages

                        String text =  String.join(";", request.substring(parts[0].length() + 1 +
                                parts[1].length() + 1));

                        try {
                            // todo, send it to another thread that will ask for the translation
                            String translatedText = Translate.execute(text, sourceLang, targetLang);

                            ACLMessage reply = msg.createReply();
                            reply.setContent(translatedText);
                            reply.setPerformative(ACLMessage.INFORM);
                            myAgent.send(reply);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                block();
            }
        });
    }

    public static Language findLang(String langDesc) {
        if (langDesc.equalsIgnoreCase("french")) {
            return Language.FRENCH;
        } else if (langDesc.equalsIgnoreCase("japanese")) {
            return Language.JAPANESE;
        } else if (langDesc.equalsIgnoreCase("english")) {
            return Language.ENGLISH;
        } else {
            return Language.fromString(langDesc);
        }
    }
}
